# MetriCal Examples

> ❗ This directory holds no data! All data needed to run these examples must be downloaded from
> [Tangram Vision's Google Drive](https://drive.google.com/drive/u/0/folders/1k58nSetIFntzhK7KA9OcfaFAK2Cxnwwi).

This directory holds common MetriCal situations, configurations, and scripts that are used in the
[Tangram Vision Documentation](https://docs.tangramvision.com). Every folder holds the input files
and dataset URL for that scenario. The correct datasets to pair with the example are linked in that
example's README.

## Configure `metrical` CLI command

All examples in this repository use the `metrical` command. This is actually an alias for a larger
Docker function, which can be found in `metrical_alias.sh`. Configure this with the right
information prior to running any examples; nothing will work otherwise!

Read the
[MetriCal Docker Alias](https://docs.tangramvision.com/metrical/intro#metrical-docker-alias)
documentation for instructions on how to configure this alias into your `.bashrc` file (or wherever
is convenient).

## Rendering

MetriCal's Calibrate and Evaluate modes have a render option (--render or -v) that allows you to
inspect the adjustment and visualize results using Rerun.

Read more about setting up the appropriate version of Rerun for your version of MetriCal in the
documentation: https://docs.tangramvision.com/metrical/intro#rendering-via-rerun

## Common Variables

| Variable    | Description                                                                                               |
| ----------- | --------------------------------------------------------------------------------------------------------- |
| `DIR`       | The directory where the data is stored. This is the directory that is mounted into the Docker container   |
| `INIT_PLEX` | The plex that was generated from our data. We'll use this as an initialization to the calibration routine |
| `OBJ`       | A description of the target(s) in the scene                                                               |
| `OUTPUT`    | The output of the calibration routine. This is a JSON file that holds the results of the calibration      |
| `REPORT`    | A report generated from MetriCal output                                                                   |
