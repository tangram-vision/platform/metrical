#!/bin/bash

############################################################
# Camera-Lidar Folders Demo
#
# This demo uses MetriCal to calibrate a Camera-lidar dataset, taken from a Velodyne and a
# RealSense.
############################################################

set -euo pipefail

# Source the `metrical` command
CURRENT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $CURRENT_DIR/../metrical_alias.sh

# Populate our variables
DIR="/datasets/camera_lidar"
DATA="$DIR/observations"
INIT_PLEX="$DIR/init_plex.json"
OBJ="$DIR/obj_circle.json"
OUTPUT="$DIR/results.json"
REPORT="$DIR/report.html"

# Initialize our Plex for the `Calibrate` mode
metrical init \
     -m infra*:opencv_radtan \
     -m velodyne*:no_offset \
     "$DATA" "$INIT_PLEX"

# Calibrate our data
metrical calibrate --report-path "$REPORT" --disable-motion-filter -o "$OUTPUT" "$DATA" "$INIT_PLEX" "$OBJ"

# Shape the calibration output into a URDF file
metrical shape urdf -a infra1_image_rect_raw "$OUTPUT" "$DIR"