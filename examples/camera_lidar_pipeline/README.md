# MetriCal Demo - Camera-LiDAR Calibration, Pipeline

This demo uses MetriCal to calibrate two cameras and a LiDAR at the same time. This time, however,
we use a pipeline file to run the calibration.

## Data

Dataset URL: https://drive.google.com/file/d/1UFKpVXNI1IZPRiupODyhU0tMedXabKEn/view?usp=drive_link

Once the zip file is extracted, place the `camera_lidar_capture.mcap` file in this directory.

## Configuration

This file assumes that you've configured the `LICENSE` variable in the `metrical_alias.sh` file.

## Running

```shell
./camera_lidar_pipeline_runner.sh
```
