#!/bin/bash

############################################################
# Camera-Lidar Folders Demo - Pipeline Version
#
# This demo uses MetriCal to calibrate a Camera-lidar dataset, taken from a Velodyne and a
# RealSense. Unlike the `camera_lidar` example, it moves all the CLI commands into the pipeline
# JSON.
############################################################

set -euo pipefail

# Source the `metrical` command
CURRENT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $CURRENT_DIR/../metrical_alias.sh

# Run the pipeline config.json
metrical pipeline /datasets/camera_lidar_pipeline/camera_lidar_pipeline.json