# Throughout the documentation, you will see references to metrical in the code snippets. This is a
# named bash function describing a larger docker command. For convenience, it can be useful to
# include that function (outlined below) in your script or shell config file (e.g. ~/.bashrc) file:

# EDIT ME: Your license key
LICENSE="YOUR_LICENSE_KEY"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Alias to run metrical in bash
metrical() {
    docker run --rm --init --user="$(id -u):$(id -g)" \
    --volume="$SCRIPT_DIR":"/datasets" \
    --workdir="/datasets" \
    --add-host=host.docker.internal:host-gateway \
    tangramvision/cli:latest \
    --license=$LICENSE \
    "$@";
    }