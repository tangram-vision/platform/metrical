#!/bin/bash

############################################################
# Multi-camera Folders Demo
#
# This demo uses MetriCal to calibrate a 3-camera dataset, taken from a RealSense 435i.
#############################################################

set -euo pipefail

# Source the `metrical` command
CURRENT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $CURRENT_DIR/../metrical_alias.sh

# Populate our variables
DIR="/datasets/rs_calibration"
DATA="$DIR/observations"
INIT_PLEX="$DIR/init_plex.json"
OBJ="$DIR/obj_markerboard.json"
OUTPUT="$DIR/results.json"
REPORT="$DIR/report.html"

# Initialize our Plex for the `Calibrate` mode
metrical init                                      \
     -m color:opencv_radtan                        \
     -m ir*:no_distortion                          \
     "$DATA" "$INIT_PLEX"

# Calibrate our data
metrical calibrate --report-path "$REPORT" --disable-motion-filter -o "$OUTPUT" "$DATA" "$INIT_PLEX" "$OBJ"

# Shape the calibration output into a URDF file
metrical shape stereo-lut --dominant ir_one --secondary ir_two "$OUTPUT" "$DIR"