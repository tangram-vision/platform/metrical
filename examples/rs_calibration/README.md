# MetriCal Demo - Multi-camera Folders Dataset

This demo uses MetriCal to calibrate a 3-camera dataset, taken from a RealSense 435i.

## Data

Dataset URL: https://drive.google.com/file/d/1Oro7CTVy7J0rc9dtgCEcBYxM20yGPa1e/view?usp=drive_link

Once the zip file is extracted, place the `observations` folder in this directory.

## Configuration

This file assumes that you've configured the `LICENSE` variable in the `metrical_alias.sh` file.

## Running

```shell
./rs_calibration_runner.sh
```
