#include "calibration_factory.h"

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <fmt/core.h>
#include <fmt/format.h>

CalibrationFactory::CalibrationFactory(json plex_, json system_)
    : plex(plex_)
    , system(system_)
{
    // fmt::println(stderr, "Plex: {}", plex);
    // fmt::println(stderr, "System: {}", system);
}

std::optional<Intrinsics> CalibrationFactory::getIntrinsics(std::string id, CameraKind kind) const
{
    if (!system[id].contains(cameraKindStr(kind))) {
        return std::nullopt;
    }

    // look up name in system
    std::string component_name = system[id][cameraKindStr(kind)];

    // look up intrinsics from name in plex
    const auto& components = plex["components"];
    const auto component
        = std::find_if(components.cbegin(), components.cend(), [&component_name](const json& c) {
              std::string name = c["camera"]["name"].get<std::string>();
              return name == component_name;
          });

    if (component == components.cend()) {
        return std::nullopt;
    }

    // Check for the right intrinsics model for RealSense
    if (!(*component)["camera"]["intrinsics"].contains("opencv_radtan")) {
        return std::nullopt;
    }

    Intrinsics intrinsics {};

    const auto& intrinsics_model = (*component)["camera"]["intrinsics"]["opencv_radtan"];
    intrinsics_model["width"].get_to(intrinsics.resolution[0]);
    intrinsics_model["height"].get_to(intrinsics.resolution[1]);
    intrinsics_model["f"].get_to(intrinsics.focalLength[0]);
    intrinsics_model["f"].get_to(intrinsics.focalLength[1]);
    intrinsics_model["cx"].get_to(intrinsics.principalPoint[0]);
    intrinsics_model["cy"].get_to(intrinsics.principalPoint[1]);
    intrinsics_model["k1"].get_to(intrinsics.distortion[0]);
    intrinsics_model["k2"].get_to(intrinsics.distortion[1]);
    intrinsics_model["p1"].get_to(intrinsics.distortion[2]);
    intrinsics_model["p2"].get_to(intrinsics.distortion[3]);
    intrinsics_model["k3"].get_to(intrinsics.distortion[4]);

    return intrinsics;
}

std::optional<Extrinsics> CalibrationFactory::getExtrinsicsFromLeft(
    std::string id, CameraKind kind) const
{
    if (!system[id].contains(cameraKindStr(kind))) {
        return std::nullopt;
    }

    Extrinsics extrinsics {};
    if (kind == CameraKind::LEFT) {
        // Exit early since LEFT from LEFT is always identity
        return extrinsics;
    }

    // look up name in system
    std::string component_name = system[id][cameraKindStr(kind)];

    // look up intrinsics from name in plex
    const auto& components = plex["components"];
    const auto other_component
        = std::find_if(components.cbegin(), components.cend(), [&component_name](const json& c) {
              return c["camera"]["name"].get<std::string>() == component_name;
          });
    if (other_component == components.cend()) {
        return std::nullopt;
    }

    auto left_name = system[id][cameraKindStr(CameraKind::LEFT)];
    const auto left_component
        = std::find_if(components.cbegin(), components.cend(), [&left_name](const json& c) {
              return c["camera"]["name"].get<std::string>() == left_name;
          });

    if (left_component == components.cend()) {
        return std::nullopt;
    }

    const auto leftUuid = (*left_component)["camera"]["uuid"];
    const auto otherUuid = (*other_component)["camera"]["uuid"];
    const auto& scs = plex["spatial_constraints"];
    const auto sc_itr
        = std::find_if(scs.begin(), scs.end(), [&leftUuid, &otherUuid](const json& sc) {
              const auto& from = sc["from"].get<std::string>();
              const auto& to = sc["to"].get<std::string>();
              return (from == leftUuid && to == otherUuid) || (from == otherUuid && to == leftUuid);
          });

    if (sc_itr == scs.end()) {
        return std::nullopt;
    }

    const auto& sc = *sc_itr;
    const auto& quat_json = sc["extrinsics"]["rotation"];
    const auto& trans_json = sc["extrinsics"]["translation"];

    // w, x, y, z
    auto quat = Eigen::Quaterniond(quat_json[3], quat_json[0], quat_json[1], quat_json[2]);
    constexpr double metersToMillimeters = 1000.0;
    auto trans = Eigen::Vector3d(trans_json[0], trans_json[1], trans_json[2]) * metersToMillimeters;
    Eigen::Isometry3d iso = Eigen::Translation3d(trans) * quat;

    const bool shouldInvert = sc["to"].get<std::string>() == leftUuid;
    if (shouldInvert) {
        iso = iso.inverse();
    }

    for (size_t i = 0, r = 0; r < 3; ++r) {
        for (size_t c = 0; c < 3; ++c, ++i) {
            extrinsics.rotationToLeft[i] = iso.matrix()(r, c);
        }
        extrinsics.translationToLeft[r] = iso.matrix()(r, 3);
    }

    return extrinsics;
}