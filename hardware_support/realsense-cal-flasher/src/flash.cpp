/**
 * @file flash.cpp
 * @author Paul Schroeder, Brandon Minor, Jeremy Steward
 * @brief Identify RealSense devices according to the provided system.json, and flash them each with
 * their respective calibrations from the derived Plex via MetriCal
 * @version 1.0
 * @date 2024-02-22
 *
 */

// Include this first, to avoid `NULL` compilation error in DSDynamicCalibration.h
#include <cstddef>

#include <DSDynamicCalibration.h>
#include <DSShared.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <argparse/argparse.hpp>
#include <chrono>
#include <cmath>
#include <filesystem>
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>
#include <fstream>
#include <iostream>
#include <librealsense2/rs.hpp>
#include <nlohmann/json.hpp>
#include <sstream>
#include <thread>

#include "calibration_factory.h"
#include "camera.h"

using nlohmann::json;

/// @brief Print the current calibration parameters
/// @param dc The current dynamic calibration object
void print_current_cal(DynamicCalibrationAPI::DSDynamicCalibration& dc)
{
    Intrinsics left {};
    Intrinsics right {};
    Intrinsics color {};
    Extrinsics leftFromLeft {};
    Extrinsics leftFromRight {};
    Extrinsics leftFromColor {};
    bool hasColor = false;

    dc.ReadCalibrationParameters(left.resolution.data(), left.focalLength.data(),
        left.principalPoint.data(), left.distortion.data(), right.focalLength.data(),
        right.principalPoint.data(), right.distortion.data(), leftFromRight.rotationToLeft.data(),
        leftFromRight.translationToLeft.data(), hasColor, color.resolution.data(),
        color.focalLength.data(), color.principalPoint.data(), color.distortion.data(),
        leftFromColor.rotationToLeft.data(), leftFromColor.translationToLeft.data());
    right.resolution[0] = left.resolution[0];
    right.resolution[1] = left.resolution[1];

    Camera cam_left { CameraKind::LEFT, left, leftFromLeft };
    Camera cam_right { CameraKind::RIGHT, right, leftFromRight };

    fmt::println(">>>> Current Calibration <<<<");
    cam_left.print(stdout);
    cam_right.print(stdout);

    if (hasColor) {
        Camera cam_color { CameraKind::COLOR, color, leftFromColor };
        cam_color.print(stdout);
    }
}

/// @brief Flash a calibration onto a device
/// @param device the RS2 device to flash
/// @param cf The calibration factory, which contains the Plex and System JSONs
/// @return 0 (or RealSense error code) if successful, 1 if not
int flash_calibration(rs2::device device, const CalibrationFactory& cf)
{
    const auto& device_id = device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    fmt::println("Flashing device {}", device_id);

    auto dc = DynamicCalibrationAPI::DSDynamicCalibration();
    dc.Initialize(&device,
        DynamicCalibrationAPI::DSDynamicCalibration::CalibrationMode::CAL_MODE_USER_CUSTOM, 1280,
        800);
    print_current_cal(dc);

    // Check for left cam
    const auto maybe_left_intrinsics = cf.getIntrinsics(device_id, CameraKind::LEFT);
    if (maybe_left_intrinsics == std::nullopt) {
        fmt::println(stderr, "Could not find left camera for device {} in plex.", device_id);
        return 1;
    }
    const auto& left_intrinsics = maybe_left_intrinsics.value();
    Camera left_cam { CameraKind::LEFT, left_intrinsics, Extrinsics {} };

    // Check for right cam
    const auto maybe_right_intrinsics = cf.getIntrinsics(device_id, CameraKind::RIGHT);
    if (maybe_right_intrinsics == std::nullopt) {
        fmt::println(stderr, "Could not find right camera for device {} in plex.", device_id);
        return 1;
    }
    const auto& right_intrinsics = maybe_right_intrinsics.value();
    const auto maybe_right_extrinsics = cf.getExtrinsicsFromLeft(device_id, CameraKind::RIGHT);
    if (maybe_right_extrinsics == std::nullopt) {
        fmt::println(
            stderr, "Could not find toLeftFromRight extrinsics for device {} in plex.", device_id);
        return 1;
    }
    const auto& right_extrinsics = maybe_right_extrinsics.value();
    Camera right_cam { CameraKind::RIGHT, right_intrinsics, right_extrinsics };

    // Check for (optional) color cam
    bool hasColor = true;
    const auto maybe_color_intrinsics = cf.getIntrinsics(device_id, CameraKind::COLOR);
    if (maybe_color_intrinsics == std::nullopt) {
        hasColor = false;
    }
    const auto& color_intrinsics = maybe_color_intrinsics.value_or(Intrinsics {});
    const auto maybe_color_extrinsics = cf.getExtrinsicsFromLeft(device_id, CameraKind::COLOR);
    if (maybe_color_extrinsics == std::nullopt) {
        hasColor = false;
    }
    const auto& color_extrinsics = maybe_color_extrinsics.value_or(invalidExtrinsics());
    Camera color_cam { CameraKind::COLOR, color_intrinsics, color_extrinsics };

    fmt::println(">>>> New Calibration <<<<");
    left_cam.print(stdout);
    right_cam.print(stdout);
    color_cam.print(stdout);

    ///////////
    // Write our calibration to the sensor
    ///////////

    const auto write_ret = dc.WriteCustomCalibrationParameters(left_intrinsics.resolution.data(),
        left_intrinsics.focalLength.data(), left_intrinsics.principalPoint.data(),
        left_intrinsics.distortion.data(), right_intrinsics.focalLength.data(),
        right_intrinsics.principalPoint.data(), right_intrinsics.distortion.data(),
        right_extrinsics.rotationToLeft.data(), right_extrinsics.translationToLeft.data(), hasColor,
        color_intrinsics.resolution.data(), color_intrinsics.focalLength.data(),
        color_intrinsics.principalPoint.data(), color_intrinsics.distortion.data(),
        color_extrinsics.rotationToLeft.data(), color_extrinsics.translationToLeft.data());

    if (write_ret == DC_SUCCESS) {
        fmt::println("Device {} - Successfully flashed calibration", device_id);
    } else if (write_ret == DC_ERROR_CUSTOM_INVALID_CAL_TABLE) {
        fmt::println(
            "Device {} - Error: One or more of the calibration tables are not valid", device_id);
    } else if (write_ret == DC_ERROR_CUSTOM_INVALID_LEFTRIGHT_RESOLUTION) {
        fmt::println(
            "Device {} - Error: Left and right camera resolution is not supported", device_id);
    } else if (write_ret == DC_ERROR_CUSTOM_LEFT_INTRINSICS_UNREASONABLE) {
        fmt::println("Device {} - Error: Left camera intrinsics unreasonable", device_id);
    } else if (write_ret == DC_ERROR_CUSTOM_RIGHT_INTRINSICS_UNREASONABLE) {
        fmt::println("Device {} - Error: Right camera intrinsics unreasonable", device_id);
    } else if (write_ret == DC_ERROR_CUSTOM_INVALID_PARAMS) {
        fmt::println("Device {} - Error: One or more of the output variables not valid", device_id);
    }

    fmt::println("----");

    return write_ret;
}

int main(int argc, char** argv)
{
    rs2::context ctx;
    rs2::device_list devices = ctx.query_devices();
    if (devices.size() == 0) {
        fmt::println(stderr, "No devices found");
        return EXIT_SUCCESS;
    }
    fmt::println("Found {} devices", devices.size());

    argparse::ArgumentParser program("rs-cal-flasher");
    program.add_argument("plex").required().help("The Plex or MetriCal results file to use");
    program.add_argument("system_config")
        .required()
        .help("The System config assigning components to RealSense Serial Numbers");

    try {
        program.parse_args(argc, argv);
    } catch (const std::exception& err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        return EXIT_FAILURE;
    }

    const auto plex_path = program.get("plex");
    const auto system_config_path = program.get("system_config");

    // Parse our plex
    std::ifstream f(plex_path);
    json plex_data = json::parse(f);
    // Determine if the input JSON is a MetriCal output or a plex file
    json plex;
    if (plex_data.contains("plex")) {
        fmt::println("JSON has a \"plex\" key so we'll interpret it as MetriCal output");
        plex = plex_data["plex"];
    } else {
        fmt::println("Interpreting JSON as a plex file, not MetriCal output");
        plex = plex_data;
    }

    // Parse our system config
    std::ifstream f_camera(system_config_path);
    json system_config = json::parse(f_camera);

    fmt::println("\nStarting flashing process for all cameras.");

    CalibrationFactory cf { plex, system_config };

    // iterate over all devices in device list
    for (const auto&& device : devices) {
        flash_calibration(device, cf);
    }

    return EXIT_SUCCESS;
}
