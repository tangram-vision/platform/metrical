/**
 * @file camera.cpp
 * @author Jeremy Steward, Brandon Minor
 * @brief A general camera struct for use by RealSense and MetriCal parsing
 * @version 0.1
 * @date 2024-02-22
 *
 */

#include "camera.h"

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ranges.h>
#include <string>
#include <string_view>

std::string cameraKindStr(CameraKind kind)
{
    switch (kind) {
    case CameraKind::LEFT:
        return std::string { "left" };
    case CameraKind::RIGHT:
        return std::string { "right" };
    case CameraKind::COLOR:
        return std::string { "color" };
    }
    return std::string { "unreachable" };
}

Extrinsics::Extrinsics()
    : rotationToLeft({ 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 })
{
}

Camera::Camera(CameraKind kind_, Intrinsics intrinsics_, Extrinsics extrinsics_) noexcept
    : kind(kind_)
    , intrinsics(intrinsics_)
    , extrinsics(extrinsics_)
{
}

const Intrinsics& Camera::getIntrinsics() const noexcept { return intrinsics; }

const Extrinsics& Camera::getExtrinsics() const noexcept { return extrinsics; }

CameraKind Camera::getCameraKind() const noexcept { return kind; }

void Camera::print(FILE* f) const
{
    fmt::println(f, "Camera Type: {}", cameraKindStr(kind));
    fmt::println(f, "\tResolution: {}", intrinsics.resolution);
    fmt::println(f, "\tFocal: {}", intrinsics.focalLength);
    fmt::println(f, "\tPrinciple Point: {}", intrinsics.principalPoint);
    fmt::println(f, "\tDistortion: {}", intrinsics.distortion);
    fmt::println(f, "\tTranslation to Left: {}", extrinsics.translationToLeft);
    fmt::println(f, "\tRotation to Left: {}", extrinsics.rotationToLeft);
}