/**
 * @file camera.h
 * @author Jeremy Steward, Brandon Minor
 * @brief A general camera struct for use by RealSense and MetriCal parsing
 * @version 0.1
 * @date 2024-02-22
 *
 */

#pragma once

#include <array>
#include <string>

/// @brief The camera type in question
enum class CameraKind { LEFT = 0, RIGHT, COLOR };

std::string cameraKindStr(CameraKind);

class CalibrationFactory;

/// @brief An intrinsics struct for a camera
struct Intrinsics {
    explicit Intrinsics() = default;

    Intrinsics(Intrinsics&) = default;
    Intrinsics(const Intrinsics&) = default;
    Intrinsics(Intrinsics&&) = default;

    Intrinsics& operator=(Intrinsics&) = default;
    Intrinsics& operator=(Intrinsics&&) = default;

    ~Intrinsics() = default;

    /// @brief The focal length of the camera (fx, fy)
    std::array<double, 2> focalLength;
    /// @brief The principal point of the camera (cx, cy)
    std::array<double, 2> principalPoint;
    /// @brief The distortion coefficients of the camera, using OpenCV's RadTan distortion model
    /// (k1, k2, p1, p2, k3)
    std::array<double, 5> distortion;
    /// @brief The resolution of the camera (width, height)
    std::array<int, 2> resolution;
};

/// @brief Extrinsics between the left camera and any other camera
struct Extrinsics {
    explicit Extrinsics();

    Extrinsics(Extrinsics&) = default;
    Extrinsics(const Extrinsics&) = default;
    Extrinsics(Extrinsics&&) = default;

    Extrinsics& operator=(Extrinsics&) = default;
    Extrinsics& operator=(Extrinsics&&) = default;

    ~Extrinsics() = default;

    /// @brief Rotation matrix to the left camera in row major orientation
    std::array<double, 9> rotationToLeft;
    /// @brief Translation vector to the left camera
    std::array<double, 3> translationToLeft;
};

inline Extrinsics invalidExtrinsics()
{
    Extrinsics e {};
    e.rotationToLeft.fill(0.0);
    return e;
}

/// @brief A general camera struct
class Camera {
public:
    explicit Camera() = delete;

    explicit Camera(CameraKind, Intrinsics, Extrinsics) noexcept;

    Camera(Camera&) = default;
    Camera(Camera&&) = default;

    Camera& operator=(Camera&) = default;
    Camera& operator=(Camera&&) = default;

    ~Camera() = default;

    const Intrinsics& getIntrinsics() const noexcept;

    const Extrinsics& getExtrinsics() const noexcept;

    CameraKind getCameraKind() const noexcept;

    void print(FILE*) const;

private:
    CameraKind kind;
    Intrinsics intrinsics;
    Extrinsics extrinsics;
};