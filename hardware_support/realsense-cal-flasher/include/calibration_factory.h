/**
 * @file calibration_factory.h
 * @author Jeremy Steward, Brandon Minor
 * @brief Create a calibrated camera object based on a Plex and System JSON
 * @version 0.1
 * @date 2024-02-22
 *
 */

#pragma once

#include <nlohmann/json.hpp>
#include <optional>

#include "camera.h"

using nlohmann::json;

class CalibrationFactory {
public:
    explicit CalibrationFactory() = delete;

    explicit CalibrationFactory(json plex_, json system_);

    CalibrationFactory(CalibrationFactory&) = default;
    CalibrationFactory(CalibrationFactory&&) = default;

    CalibrationFactory& operator=(CalibrationFactory&) = default;
    CalibrationFactory& operator=(CalibrationFactory&&) = default;

    ~CalibrationFactory() = default;

    std::optional<Intrinsics> getIntrinsics(std::string, CameraKind) const;

    std::optional<Extrinsics> getExtrinsicsFromLeft(std::string, CameraKind) const;

private:
    json plex;
    json system;
};