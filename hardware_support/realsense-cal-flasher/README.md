# MetriCal-to-RealSense Calibration Flasher

This utility takes the values from a calibrated Plex from Tangram Vision's
[MetriCal](https://gitlab.com/tangram-vision/platform/metrical) and flashes them onto one (or many)
Intel RealSense.

> 🦀 Check out the [RealSense-Rust] crate, maintainted by Tangram Vision, to easily record
> calibration data to a ROSbag using the `record_bag` example.

> 🐧 This tool has only been tested on Linux machines. Those using other operating systems should
> use discretion.

## Setup

### Dependency - RealSense

This program expects that librealsense2 is installed on your local machine. We recommend using the
pre-built package for
[Linux](https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md).

### Dependency - RealSense Dynamic Calibration Tool

This program will look for the
[Intel® RealSense™ D400 Series Dynamic Calibration Tool](https://www.intel.com/content/www/us/en/download/645988/intel-realsense-d400-series-dynamic-calibration-tool.html)
on your local machine. Make sure to download and install this utility before attempting to build the
program.

This program will look for the utility at this location by default:

```shell
/usr/share/doc/librscalibrationtool/api/DynamicCalibrationAPI-Linux-2.13.1.0.tar.gz
```

One can change this location by modifying the CMakeLists.txt.

### Build

```shell
cd <this directory>
mkdir build && cd build
cmake ..
make
```

## Usage

```shell
./flash_calibration <path_to_plex>.json <path_to_camera_config>.json
```

Make sure that your RealSense is plugged in!

## Command Line Arguments

### Plex Structure

A Plex is a JSON file produced by Tangram Vision's calibration software,
[MetriCal](https://gitlab.com/tangram-vision/platform/metrical). It contains the full calibration
state of your system at the time of processing. One can produce a plex from a MetriCal output by
running the following [jq](https://jqlang.github.io/jq/) command:

```shell
less output.json | jq .plex > plex.json
```

### Sensor Config

This RealSense Flasher tool, as written, will look for any camera components for every RealSense
connected as described by the camera config passed into the CLI:

-   "left": The leftmost IR camera on the RealSense, when facing the line of sight of the cameras
-   "right": The rightmost IR camera on the RealSense
-   "color" (optional): The color camera on the RealSense

The form of the camera config is below. One can also find an example multi-RS config in the
`fixtures` folder.

```json
{
  "sensor_serial_one": {
    "left": "name_of_the_left_component_in_plex",
    "right": "name_of_the_right_component_in_plex"
  },
  "sensor_serial_two" {
    ...
  }
  ...
}
```

## Error Codes

All taken from the
[Dynamic Calibration Programmer documentation for RealSense](https://www.intel.com/content/dam/support/us/en/documents/emerging-technologies/intel-realsense-technology/RealSense_D400_Dyn_Calib_Programmer.pdf).

| Code                                          | Value | Meaning                                             |
| --------------------------------------------- | ----- | --------------------------------------------------- |
| DC_SUCCESS                                    | 0     | Calibration applied successfully                    |
| DC_ERROR_CUSTOM_INVALID_CAL_TABLE             | 2000  | One or more of the calibration tables are not valid |
| DC_ERROR_CUSTOM_INVALID_LEFTRIGHT_RESOLUTION  | 2001  | Left and right camera resolution is not supported   |
| DC_ERROR_CUSTOM_LEFT_INTRINSICS_UNREASONABLE  | 2002  | Left camera intrinsics unreasonable                 |
| DC_ERROR_CUSTOM_RIGHT_INTRINSICS_UNREASONABLE | 2003  | Right camera intrinsics unreasonable                |

## Further Sources

These write-ups by Intel go into many of the nitty-gritty details of the RealSense calibration
ecosystem.

-   The RealSense calibration white paper:
    https://dev.intelrealsense.com/docs/d400-series-custom-calibration-white-paper
-   The Dynamic Calibration Programmer documentation for RealSense:
    https://www.intel.com/content/dam/support/us/en/documents/emerging-technologies/intel-realsense-technology/RealSense_D400_Dyn_Calib_Programmer.pdf
