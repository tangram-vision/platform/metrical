# Spatial Constraint Traversal

Use a pythonic `Plex` class to traverse spatial constraints in a plex or MetriCal results file
correctly.

## Not All Constraints Are Created Equal

Since all spatial constraints in a plex form an undirected (maybe even fully connected) graph, it
can be confusing to figure out how to traverse that graph to find the "best" extrinsics between two
components. MetriCal itself provides the
[`Shape` mode](https://docs.tangramvision.com/metrical/modes/shape) to help with this (with a few
helpful options in
[`Pretty Print`](https://docs.tangramvision.com/metrical/modes/pretty_print#options)), but sometimes
it's useful to do your own thing.

`Plex.py` demonstrates the right way to derive optimal extrinsics via the magic of python. Feel free
to riff on this for your own codebase; this is an open-source Utility project, after all! Just make
sure that your results are consistent with those returned by MetriCal's `Shape` mode and the
`Plex::get_extrinsics` method provided here.

## Spatial Constraint Traversal Example

This script replicates the functionality of metrical's pretty-print command. Along with Plex.py, it
gives a detailed look into how the ideal extrinsics are calculated from a plex file.

Run the script with the following command:

```shell
python3 traversal_example.py <plex_or_results_file> --to <to_component> --from <from_component>
```

Compare the results of this script with the output of the `pretty-print` command in MetriCal:

```shell
metrical pretty-print <plex_or_results_file> -a <to_component> -b <from_component>
```

## Running Unit Tests

```shell
pytest test_plex.py
```
