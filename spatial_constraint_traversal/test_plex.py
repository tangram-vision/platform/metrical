import pytest
from plex import Plex
import os
import numpy as np


#
# This should be the optimal path for all spatial constraints, given the covariances in the
# test_plex.json file.
#
# A--B
# |  |
# C  |
#    |
#    D
#
@pytest.fixture
def test_plex():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    plex = Plex(dir_path + "/test_plex.json")
    return plex


class TestPlex:

    def test_plex_traversal(self, test_plex):
        # Assert that all of our paths have the right connection count
        assert len(test_plex.get_shortest_path("C", "A")) == 1
        assert len(test_plex.get_shortest_path("A", "B")) == 1
        assert len(test_plex.get_shortest_path("B", "D")) == 1

        path_c_a = test_plex.get_extrinsics("C", "A")
        path_a_b = test_plex.get_extrinsics("A", "B")
        path_b_d = test_plex.get_extrinsics("B", "D")
        traversed_path_c_d = path_c_a @ path_a_b @ path_b_d

        assert len(test_plex.get_shortest_path("C", "D")) == 3
        path_c_d = test_plex.get_extrinsics("C", "D")

        np.testing.assert_allclose(traversed_path_c_d, path_c_d, rtol=1e-10, atol=1e-10)

    def test_inverse_traversal(self, test_plex):
        assert len(test_plex.get_shortest_path("C", "A")) == 1
        assert len(test_plex.get_shortest_path("A", "C")) == 1

        path_c_a = test_plex.get_extrinsics("C", "A")
        path_a_c = test_plex.get_extrinsics("A", "C")
        np.testing.assert_allclose(
            path_c_a, np.linalg.inv(path_a_c), rtol=1e-10, atol=1e-10
        )

        # Make sure that CA does not equal AC
        with pytest.raises(AssertionError) as exc:
            np.testing.assert_allclose(path_c_a, path_a_c, rtol=1e-10, atol=1e-10)
        assert exc.type == AssertionError

        assert len(test_plex.get_shortest_path("C", "B")) == 2
        assert len(test_plex.get_shortest_path("B", "C")) == 2

        path_c_b = test_plex.get_extrinsics("C", "B")
        path_b_c = test_plex.get_extrinsics("B", "C")
        np.testing.assert_allclose(
            path_c_b, np.linalg.inv(path_b_c), rtol=1e-10, atol=1e-10
        )
