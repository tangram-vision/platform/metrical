"""
File: plex.py
Description: Lightweight python implementation of extrinsics derivation from a Plex structure
"""

from scipy.spatial.transform import Rotation as R
import igraph as ig
import json
import numpy as np


def gather_extrinsics(data, names, uuids):
    """
    Gather extrinsics from the plex data
    """
    edges = []
    weights = []
    tos = []
    froms = []
    transforms = []
    for item in data["spatial_constraints"]:
        # Create our edge by looking up the right name
        to_name = names[uuids.index(item["to"])]
        from_name = names[uuids.index(item["from"])]
        pos_to = names.index(to_name)
        pos_from = names.index(from_name)
        edges.append((pos_to, pos_from))
        tos.append(to_name)
        froms.append(from_name)
        # Populate its weight as the covariance determinant
        covariance_det = np.linalg.det(
            np.array(item["covariance"]["raw_se3"]).reshape(6, 6)
        )
        weights.append(covariance_det)
        # Populate its extrinsics
        translation = item["extrinsics"]["translation"]
        rot_quaternion = item["extrinsics"]["rotation"]
        rotation = R.from_quat(rot_quaternion)
        transform = np.identity(4)
        transform[:3, :3] = rotation.as_matrix()
        transform[:3, 3] = translation
        transforms.append(transform)
    return (edges, weights, tos, froms, transforms)


class Plex(object):
    """
    Plex class to store and manipulate the plex data. It uses the igraph library to store the graph.
    """

    def __init__(self, plex_or_results_file):
        with open(plex_or_results_file, "r") as file:
            data = json.load(file)

        # The "plex" key is only in MetriCal results files. Otherwise, this file assumes that it's
        # been given a plex file to begin with.
        if "plex" in data:
            data = data["plex"]

        names = [
            component["name"]
            for item in data["components"]
            for component in item.values()
        ]
        uuids = [
            component["uuid"]
            for item in data["components"]
            for component in item.values()
        ]

        (edges, weights, tos, froms, transforms) = gather_extrinsics(data, names, uuids)
        self.plex = ig.Graph(n=len(names), edges=edges)
        self.names = names
        self.plex.vs["name"] = names
        self.plex.es["weight"] = weights
        self.plex.es["to"] = tos
        self.plex.es["from"] = froms
        self.plex.es["transform"] = transforms

    def get_shortest_path(self, component_to, component_from):
        """
        Find the shortest path between two components by edge idx. If we have more than one eligible
        path, this function just returns the first.
        """
        return self.plex.get_shortest_paths(
            self.names.index(component_from),
            to=self.names.index(component_to),
            weights=self.plex.es["weight"],
            output="epath",  # get edge IDs for traversal
        )[0]

    def get_extrinsics(self, component_to, component_from):
        """
        Find the shortest path between two components, and therefore the optimal extrinsics solution.
        """
        shortest_path = self.get_shortest_path(component_to, component_from)

        # If there is no path, return the identity matrix
        curr_transform = np.identity(4)
        if shortest_path[0] == []:
            return curr_transform

        # Initialize our transforms
        if self.plex.es[shortest_path[0]]["from"] == component_from:
            curr_to = self.plex.es[shortest_path[0]]["to"]
            curr_transform = self.plex.es[shortest_path[0]]["transform"]
        else:
            curr_to = self.plex.es[shortest_path[0]]["from"]
            curr_transform = np.linalg.inv(self.plex.es[shortest_path[0]]["transform"])

        # Run the shortest path
        for path in shortest_path[1::]:
            next_transform = self.plex.es[path]["transform"]
            if self.plex.es[path]["from"] == curr_to:
                curr_transform = next_transform @ curr_transform
                curr_to = self.plex.es[path]["to"]
            else:
                curr_transform = np.linalg.inv(next_transform) @ curr_transform
                curr_to = self.plex.es[path]["from"]

        return curr_transform
