"""
File: traversal_example.py
Description: Example of how to use the plex class to get extrinsics between components.

This script replicates the functionality of metrical's pretty-print command:

```shell
metrical pretty-print <plex_or_results_file> -a <to_component> -b <from_component>
```

Along with Plex.py, it gives a detailed look into how the ideal extrinsics are calculated from a
plex file.
"""

import argparse
from plex import Plex
from scipy.spatial.transform import Rotation as R


def print_extrinsics(to_c, from_c, plex):
    print('Extrinsics to {} from {} ("To" is origin):'.format(to_c, from_c))
    extrinsics = plex.get_extrinsics(to_c, from_c)
    rot_deg = R.from_matrix(extrinsics[:3, :3]).as_euler("xyz", degrees=True)
    rot_rad = R.from_matrix(extrinsics[:3, :3]).as_euler("xyz", degrees=False)
    print("Rotation (deg): ", rot_deg)
    print("Rotation (rad): ", rot_rad)
    print("Translation (m): ", extrinsics[0:3, 3])


if __name__ == "__main__":
    global plex
    parser = argparse.ArgumentParser(
        description="Get extrinsics between plex components."
    )
    parser.add_argument(
        "plex_or_results_file",
        type=str,
        help="Path to the plex or results file to parse.",
    )
    parser.add_argument(
        "--to", type=str, dest="to_component", help="The To (or origin) component."
    )
    parser.add_argument(
        "--from", type=str, dest="from_component", help="The From component."
    )
    args = parser.parse_args()
    plex = Plex(args.plex_or_results_file)
    print("All available components in the plex:", *plex.names, "\n", sep="\n")

    if args.to_component and args.from_component:
        print_extrinsics(args.to_component, args.from_component, plex)
    else:
        print(
            "Please specify the --to and --from components for output of extrinsics to console."
        )
        exit(1)
