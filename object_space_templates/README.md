# Object Space Template Library

Object space describes any targets or fiducials that we expect to see in a calibration dataset.
These can be checkerboards, markerboards (ChArUco, for example), AprilGrids, or custom fiducials
like our Circular Camera-Lidar target.

Full explanations on the content and use of these object spaces can be found in the
[Tangram Vision Documentation](https://docs.tangramvision.com).

It’s required to pass an object space JSON to MetriCal during a calibration run. This tells MetriCal
what detectors to use, and what kind of observations to expect.
