# MetriCal Utilities: Tutorials and Code Resources

> 🤔 This repository doesn't hold MetriCal; the program itself is distributed via
> [Docker Hub](https://hub.docker.com/r/tangramvision/cli). This repository serves as a Resource to
> exploring MetriCal's capabilities, as well as a place to leave feedback and suggestions.

> 🚨 Running MetriCal requires a license key. Get a license key by contacting the Tangram Vision
> team at <metrical@tangramvision.com>.

## Introduction - Multimodal Calibration for Robotics and Autonomy

MetriCal is a sophisticated global bundle adjustment software specifically built to give accurate,
precise, and expedient calibration results for multimodal sensor suites. Its easy-to-use interface
and detailed metrics enable enterprise-level autonomy at scale.

> 📚 You can find thorough documentation at <https://docs.tangramvision.com>.

### Capabilities

-   Process camera + lidar + IMU streams simultaneously.
-   No restriction to the number of streams processed.
-   Get visual and quantitative feedback on data input quality and output metrics.
-   Process ROSbags, MCAP files, and folder datasets.
-   Convert a calibration file into a URDF file for easy integration into ROS.
-   Use a variety of fiducials and targets.
-   Create pixel-wise lookup tables for both single camera correction and stereo pair rectification.

### Available Camera Models

-   `"no_distortion"`: No distortion model applied, i.e. an ideal pinhole model
-   `"opencv_radtan"`: [OpenCV RadTan](https://docs.opencv.org/4.x/d9/d0c/group__calib3d.html)
-   `"opencv_fisheye"`:
    [OpenCV Fisheye](https://docs.opencv.org/4.x/db/d58/group__calib3d__fisheye.html)
-   `"opencv_rational"`: [OpenCV Rational](https://docs.opencv.org/4.x/d9/d0c/group__calib3d.html),
    an extension of OpenCV RadTan with radial terms in the denominator
-   `"pinhole_with_brown_conrady"`:
    [Inverse Kannala-Brandt, correction in image space](https://oulu3dvision.github.io/calibgeneric/Kannala_Brandt_calibration.pdf)
-   `"pinhole_with_kannala_brandt"`:
    [Inverse Brown-Conrady, correction in image space](https://academic.oup.com/mnras/article/79/5/384/1078771?login=false)
-   `"eucm"`: [Enhanced Unified Camera Model, i.e. EUCM](https://hal.science/hal-01722264/document)
-   `"double_sphere"`: [Double Sphere](https://arxiv.org/abs/1807.08957)
-   `"omni"`:
    [Omnidirectional Camera Model](https://www.robots.ox.ac.uk/~cmei/articles/single_viewpoint_calib_mei_07.pdf)

### Available LiDAR Models

-   `"no_offset"`: Factory intrinsics.

### Available IMU Models

-   `"no_intrinsics"`: A generic consumer-grade IMU model.
-   `"scale"`: IMU with a scale factor applied to the gyroscope and accelerometer.
-   `"scale_shear"`: IMU with a scale factor and shear applied to the gyroscope and accelerometer.
-   `"scale_shear_rotation"`: IMU with a scale factor and shear applied to the gyroscope and
    accelerometer, as well as a rotation between the accelerometer and gyroscope.
-   `"scale_shear_rotation_g_sensitivity"`: All of the above, as well as solving for G sensitivity
    in the gyroscope.

### Available Device Presets

-   RealSense 415
-   RealSense 435
-   RealSense 435i
-   RealSense 455

> 🎮 Passing in a device may help MetriCal initialize a calibration; however, the calibration's
> final result will only be informed by the input data.

## We want to hear from you!

Check out the Gitlab Issues in this repository to see planned features and fixes. If you have a
suggestion for MetriCal functionality, or see something missing, let us know there! You might find
it's already listed, in which case you should leave a 👍 on the issue so that we can prioritize its
addition.

## Further Support

If you should need any further support, please contact us at <support@tangramvision.com>.
