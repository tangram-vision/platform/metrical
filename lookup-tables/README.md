# MetriCal Lookup Tables

This directory holds examples of different image manipulations using per-pixel look up tables, or
LUTs.

## Image Rectification and Correction Look Up Tables

Tangram's image correction look up tables (LUT) are compatible with with
[opencv's remap](https://docs.opencv.org/4.7.0/d1/da0/tutorial_remap.html) function. This directory
provides examples of how to load in Tangram's LUTs and use them to correct an image.

See the folder corresponding to your language of choice for how to use our LUTs in your program.
