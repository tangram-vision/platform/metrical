import argparse
import cv2
import json
import numpy as np
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_image_left")
    parser.add_argument("input_image_right")
    parser.add_argument("lut_left")
    parser.add_argument("lut_right")
    args = parser.parse_args()

    # Read the image
    img_left = cv2.imread(args.input_image_left, cv2.IMREAD_GRAYSCALE)
    img_right = cv2.imread(args.input_image_right, cv2.IMREAD_GRAYSCALE)

    # Make sure our images are the same size
    assert img_left.shape == img_right.shape

    # Parse the LUT
    f_left = open(args.lut_left)
    f_right = open(args.lut_right)

    lut_left = json.loads(f_left.read())
    lut_right = json.loads(f_right.read())
    np_lut_left = np.array(lut_left, dtype=np.float32)
    np_lut_right = np.array(lut_right, dtype=np.float32)

    # Note: OpenCV uses 0,0 in their remap tables as the center of the
    # top-left pixel. Tangram LUTs are generated using the same convention.
    # As such, no additional 0.5 pixel offset is needed here.
    map_x_left = np_lut_left[:, 0].reshape((img_left.shape[0], img_left.shape[1]))
    map_y_left = np_lut_left[:, 1].reshape((img_left.shape[0], img_left.shape[1]))
    corrected_img_left = cv2.remap(img_left, map_x_left, map_y_left, cv2.INTER_LINEAR)

    map_x_right = np_lut_right[:, 0].reshape((img_right.shape[0], img_right.shape[1]))
    map_y_right = np_lut_right[:, 1].reshape((img_right.shape[0], img_right.shape[1]))
    corrected_img_right = cv2.remap(
        img_right, map_x_right, map_y_right, cv2.INTER_LINEAR
    )
    print(corrected_img_right.shape)

    # Create stereo block matching object
    stereo = cv2.StereoSGBM_create(numDisparities=128, blockSize=21)

    # Compute the disparity map
    disparity = stereo.compute(corrected_img_left, corrected_img_right)

    # Normalize the disparity map for better visualization
    disparity = cv2.normalize(
        disparity, disparity, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX
    )
    disparity = np.uint8(disparity)

    # Display the result
    display_scale = 0.5
    width = int(display_scale * img_left.shape[1])
    height = int(display_scale * img_left.shape[0])
    small_disparity = cv2.resize(disparity, (width, height))
    cv2.imshow("Disparity Map", small_disparity)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
