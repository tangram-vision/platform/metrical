import argparse
import cv2
import json
import numpy as np
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_image")
    parser.add_argument("lut")
    args = parser.parse_args()

    # Read the image
    img = cv2.imread(args.input_image)

    # Parse the LUT
    f = open(args.lut)
    lut = json.loads(f.read())
    np_lut = np.array(lut["lut"], dtype=np.float32)

    # Note: OpenCV uses 0,0 in their remap tables as the center of the
    # top-left pixel. Tangram LUTs are generated using the same convention.
    # As such, no additional 0.5 pixel offset is needed here.
    map_x = np_lut[:, 0].reshape((img.shape[0], img.shape[1]))
    map_y = np_lut[:, 1].reshape((img.shape[0], img.shape[1]))
    corrected_img = cv2.remap(img, map_x, map_y, cv2.INTER_LINEAR)

    # Print out camera matrix
    print(lut["camera"]["intrinsics"]["projection"])

    imgs = np.concatenate((img, corrected_img), axis=1)
    display_scale = 0.25
    width = int(0.25 * imgs.shape[1])
    height = int(0.25 * imgs.shape[0])
    display = cv2.resize(imgs, (width, height))
    cv2.imshow("Corrected Image", display)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
