# JSON Schema for MetriCal Inputs and Outputs

Use these JSON Schema to both verify your inputs/outputs and more easily inspect the data within
these structures.

Full explanations on the content and use of these schema can be found in the
[Tangram Vision Documentation](https://docs.tangramvision.com).

NOTE: Currently, MetriCal inputs and outputs are _not versioned_. There is no guarantee of backwards
compatibility between major versions of MetriCal.
